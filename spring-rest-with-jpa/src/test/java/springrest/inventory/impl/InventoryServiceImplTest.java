package springrest.inventory.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.CoreMatchers;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.data.domain.Example;

import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import mockit.internal.expectations.transformation.ExpectationsTransformer;
import springrest.RecordNotFoundException;
import springrest.model.Pricing;
import springrest.model.PricingModel;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.repo.RoomCategoryRepository;
import springrest.repo.RoomRepository;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InventoryServiceImplTest {

	@Tested
	private InventoryServiceImpl inventoryServiceImpl;
	@Injectable
	RoomCategoryRepository roomCategoryRepository;
	@Injectable
	RoomRepository roomRepository;

	private List<RoomCategory> getRoomCategoryList() {
		List<RoomCategory> roomCategoryList = new ArrayList<RoomCategory>();
		RoomCategory roomCategory1 = new RoomCategory();
		roomCategory1.setId(1);
		roomCategory1.setName("Single");
		roomCategory1.setDescription("only one person can stay");
		Pricing pricing1 = new Pricing(PricingModel.FIXED);
		pricing1.setId(1);
		pricing1.setPriceGuest1(5000.00);
		pricing1.setPriceGuest2(6000.00);
		pricing1.setPriceGuest3(8000.00);
		roomCategory1.setPricing(pricing1);
		RoomCategory roomCategory2 = new RoomCategory();
		roomCategory2.setId(2);
		roomCategory2.setName("Double");
		roomCategory2.setDescription("only two persons can stay");
		Pricing pricing2 = new Pricing(PricingModel.SLIDING);
		pricing2.setId(2);
		pricing2.setPriceGuest1(6000.00);
		pricing2.setPriceGuest2(7000.00);
		pricing2.setPriceGuest3(8000.00);
		roomCategory2.setPricing(pricing2);
		roomCategoryList.add(roomCategory1);
		roomCategoryList.add(roomCategory2);

		return roomCategoryList;
	}

	private List<Room> getRoomList() {
		List<Room> roomList = new ArrayList<Room>();
		Room room1 = new Room();
		room1.setId(1);
		room1.setName("101");
		room1.setDescription("non ac");
		RoomCategory roomCategory1 = new RoomCategory();
		roomCategory1.setId(1);
		roomCategory1.setName("single");
		roomCategory1.setDescription("only one person can stay");
		Pricing pricing1 = new Pricing(PricingModel.FIXED);
		pricing1.setId(1);
		pricing1.setPriceGuest1(5000.00);
		pricing1.setPriceGuest2(7000.00);
		pricing1.setPriceGuest3(6000.00);
		roomCategory1.setPricing(pricing1);
		room1.setRoomCategory(roomCategory1);
		Room room2 = new Room();
		room2.setId(2);
		room2.setName("102");
		room2.setDescription("ac");
		RoomCategory roomCategory2 = new RoomCategory();
		roomCategory2.setId(2);
		roomCategory2.setName("single");
		roomCategory2.setDescription("only one person can stay");
		Pricing pricing2 = new Pricing(PricingModel.FIXED);
		pricing2.setId(2);
		pricing2.setPriceGuest1(5000.00);
		pricing2.setPriceGuest2(7000.00);
		pricing2.setPriceGuest3(6000.00);
		roomCategory2.setPricing(pricing2);
		room2.setRoomCategory(roomCategory2);
		roomList.add(room1);
		roomList.add(room2);
		return roomList;
	}
     //testcase for addRoomCategory
	@Test
	public void testAddRoomCategory() {
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setId(0);
		roomCategory.setName("Single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(6000.00);
		pricing.setPriceGuest3(8000.00);
		roomCategory.setPricing(pricing);
		new Expectations() {
			{
				roomCategoryRepository.save(roomCategory);
			}
		};
		inventoryServiceImpl.addRoomCategory(roomCategory);
		new Verifications() {
			{
				assertEquals("Single", roomCategory.getName());
			}
		};
	}
     //testcase for updateRoomCategory
	@Test
	public void testUpdateRoomCategory() {
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setId(1);
		roomCategory.setName("Double");
		roomCategory.setDescription("only two persons can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(8000.00);
		pricing.setPriceGuest2(9000.00);
		pricing.setPriceGuest3(10000.00);
		roomCategory.setPricing(pricing);

		new Expectations() {
			{
				roomCategoryRepository.save(roomCategory);
				returns(roomCategory);
				times = 1;

			}
		};
		inventoryServiceImpl.updateRoomCategory(roomCategory);
		new Verifications() {
			{
				assertEquals("Double", roomCategory.getName());
			}
		};

	}

	@Test(expected = IllegalArgumentException.class)
	public void testCheckPricing1() {
		RoomCategory category1 = new RoomCategory();
		category1.setId(100);
		category1.setName("single with ac");
		category1.setDescription("only one person can stay");
		category1.setPricing(null);
		Deencapsulation.invoke(inventoryServiceImpl, "checkPricing", category1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCheckPricing2() {
		RoomCategory category1 = new RoomCategory();
		category1.setId(100);
		category1.setName("single with ac");
		category1.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(null);
		pricing.setPriceGuest2(8000.00);
		pricing.setPriceGuest3(10000.00);
		category1.setPricing(pricing);
		Deencapsulation.invoke(inventoryServiceImpl, "checkPricing", category1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCheckPricing3() {
		RoomCategory category1 = new RoomCategory();
		category1.setId(100);
		category1.setName("single with ac");
		category1.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.SLIDING);
		pricing.setId(1);
		pricing.setPriceGuest1(7000.00);
		pricing.setPriceGuest2(null);
		pricing.setPriceGuest3(10000.00);
		category1.setPricing(pricing);
		Deencapsulation.invoke(inventoryServiceImpl, "checkPricing", category1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetRoomCategory1() {
		RoomCategory category1 = new RoomCategory();
		category1.setId(0);
		category1.setName("single");
		category1.setDescription("only one person can stay");
		RoomCategory roomCategory = inventoryServiceImpl.getRoomCategory(category1.getId());
		assertEquals(0, roomCategory.getId());
	}

	@Test
	public void testGetRoomCategory2() {
		RoomCategory roomCategory = new RoomCategory();
		Optional<RoomCategory> optional = Optional.of(roomCategory);
		roomCategory.setId(1);
		roomCategory.setName("Single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(6000.00);
		pricing.setPriceGuest3(8000.00);
		roomCategory.setPricing(pricing);
		new Expectations() {
			{
				roomCategoryRepository.findById(roomCategory.getId());
				returns(optional);
				times = 1;
			}
		};
		RoomCategory roomCate = inventoryServiceImpl.getRoomCategory(optional.get().getId());
		new Verifications() {
			{
				assertEquals(1, roomCate.getId());
			}
		};
	}

	@Test(expected = RecordNotFoundException.class)
	public void testGetRoomCategory3() {
		RoomCategory roomCategory = null;
		Optional<RoomCategory> optional = Optional.empty();

		new Expectations() {
			{
				roomCategoryRepository.findById(anyLong);
				returns(optional);
				times = 1;
			}
		};
		RoomCategory roomCate = inventoryServiceImpl.getRoomCategory(2l);
		assertEquals(null, roomCate);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDeleteRoomCategory1() {
		RoomCategory category1 = new RoomCategory();
		category1.setId(0);
		category1.setName("single");
		category1.setDescription("only one person can stay");
		inventoryServiceImpl.deleteRoomCategory(category1.getId());
		assertEquals("single", category1.getName());
	}

	@Test
	public void testDeleteRoomCategory2(@Mocked RoomCategoryRepository rcr) {
		RoomCategory roomCategory = new RoomCategory();
		Optional<RoomCategory> category = Optional.of(roomCategory);
		roomCategory.setId(1);
		roomCategory.setName("Single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(6000.00);
		pricing.setPriceGuest3(8000.00);
		roomCategory.setPricing(pricing);
		new Expectations() {
			{
				roomCategoryRepository.findById(roomCategory.getId());
				returns(category);
				times = 1;
				rcr.delete(roomCategory);
			}
		};
		inventoryServiceImpl.deleteRoomCategory(roomCategory.getId());
		  new Verifications() {{
			  rcr.delete(roomCategory); times=1;
		  }};
	}

	@Test
	public void testGetAllRoomCategories() {
		List<RoomCategory> roomCategoryList = getRoomCategoryList();
		new Expectations() {
			{
				roomCategoryRepository.findAll();
				returns(roomCategoryList);
				times = 1;
			}
		};
		List<RoomCategory> roomList = inventoryServiceImpl.getAllRoomCategories();
		assertEquals(2, roomList.size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddRoom1() {
		Room room = new Room();
		room.setId(2);
		room.setName("101");
		inventoryServiceImpl.addRoom(room);
		assertEquals(null, room);
	}

	@Test
	public void testAddRoom2() {
		Room room = new Room();
		room.setId(0);
		room.setName("101");
		room.setDescription("non ac");
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setId(1);
		roomCategory.setName("single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(7000.00);
		pricing.setPriceGuest3(6000.00);
		roomCategory.setPricing(pricing);
		room.setRoomCategory(roomCategory);
		new Expectations() {
			{
				roomRepository.save(room);
			}
		};
		inventoryServiceImpl.addRoom(room);
		new Verifications() {
			{
				assertEquals("101", room.getName());
			}
		};
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetRoom1() {
		Room room = new Room();
		room.setId(0);
		room.setName("101");
		inventoryServiceImpl.getRoom(room.getId());
		assertEquals(null, room);
	}

	@Test
	public void testGetRoom2() {
		Room room = new Room();
		Optional<Room> optional = Optional.of(room);
		room.setId(1);
		room.setName("101");
		room.setDescription("non ac");
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setId(1);
		roomCategory.setName("single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(7000.00);
		pricing.setPriceGuest3(6000.00);
		roomCategory.setPricing(pricing);
		room.setRoomCategory(roomCategory);
		new Expectations() {
			{
				roomRepository.findById(optional.get().getId());
				returns(optional);
				times = 1;
			}
		};
		Room roomObj = inventoryServiceImpl.getRoom(room.getId());
		new Verifications() {
			{
				assertEquals("101", roomObj.getName());
			}
		};
	}

	@Test(expected = RecordNotFoundException.class)
	public void testGetRoom3() {
		Room room = new Room();
		Optional<Room> optional = Optional.empty();
		new Expectations() {
			{
				roomRepository.findById(anyLong);
				returns(optional);
				times = 1;
			}
		};
		Room roomObj = inventoryServiceImpl.getRoom(2l);
		new Verifications() {
			{
				assertEquals(null, roomObj);
			}
		};
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUpdateRoom1() {
		Room room = new Room();
		room.setId(0);
		inventoryServiceImpl.updateRoom(room);
		assertEquals(null, room);
	}

	@Test
	public void testUpdateRoom2() {
		Room room = new Room();
		room.setId(1);
		room.setName("102");
		room.setDescription("non ac");
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setId(1);
		roomCategory.setName("single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(7000.00);
		pricing.setPriceGuest3(6000.00);
		roomCategory.setPricing(pricing);
		room.setRoomCategory(roomCategory);
		new Expectations() {
			{
				roomRepository.save(room);
			}
		};
		inventoryServiceImpl.updateRoom(room);
		new Verifications() {
			{
				assertEquals("102", room.getName());
			}
		};
	}

	@Test
	public void testGetAllRooms() {
		List<Room> roomList = getRoomList();
		new Expectations() {
			{
				roomRepository.findAll();
				returns(roomList);
				times = 1;
			}
		};
		List<Room> roomList1 = inventoryServiceImpl.getAllRooms();
		assertEquals(2, roomList1.size());
	}
	@Test
	public void testGetAllRoomsWithCategory() {
		RoomCategory category = new RoomCategory();
		category.setId(1);
		category.setName("single");
		category.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(7000.00);
		pricing.setPriceGuest3(6000.00);
		category.setPricing(pricing);
		Room r = new Room();
		r.setId(1);
		r.setName("102");
		r.setDescription("non ac");
		r.setRoomCategory(category);
		Example<Room> matchingRooms = Example.of(r);
		List<Room> rooooms = inventoryServiceImpl.getAllRoomsWithCategory(category);
		assertEquals(0, rooooms.size());

	}

}

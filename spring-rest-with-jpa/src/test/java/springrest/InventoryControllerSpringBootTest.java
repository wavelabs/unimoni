package springrest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import springrest.inventory.InventoryService;
import springrest.inventory.web.RoomDTO;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.repo.RoomRepository;

import java.util.Optional;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class InventoryControllerSpringBootTest {

  @Autowired
  private MockMvc mvc;

  @SpyBean
  private InventoryService inventoryService;

  @MockBean
  private RoomRepository roomRepository;

  @Test
  void testContextLoads() {

  }

  @Test
  void getRoom() throws Exception {

    // mocking behavior to return data by repository
    given(roomRepository.findById(1L))
        .willReturn(Optional.of(new Room().setId(1).setRoomCategory(new RoomCategory().setId(1))));

    mvc.perform(get("/rooms/1").accept(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print()) // print the request and response
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasKey("status")))
        .andExpect(jsonPath("$", hasEntry("status", "OK")))
        .andExpect(jsonPath("$", hasKey("data")))
        .andExpect(jsonPath("$", hasEntry("errors", null)))
        .andExpect(jsonPath("$", hasEntry("error", null)))
//        .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(1))))
        .andExpect(jsonPath("$.data.id", is(1)))
        .andExpect(jsonPath("$.data.roomCategoryId", is(1)));

    // validating number of time the method is called
    verify(inventoryService, atLeastOnce()).getRoom(1L);
    verify(roomRepository, atLeastOnce()).findById(1L);

  }

  //  @Test
  @DisplayName("Assert Validation Errors")
  void addRoom() throws Exception {
    RoomDTO room = new RoomDTO()
        .setName("TR")
        .setDescription("Room Desc")
        .setRoomCategoryId(2);

    mvc.perform(post("/rooms").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(room)))
        .andDo(print())
        .andExpect(status().is4xxClientError()) // validation error
        .andExpect(jsonPath("$.errors", notNullValue()))
        .andExpect(jsonPath("$.errors", hasSize(greaterThanOrEqualTo(1)))) // one or more errors
        .andExpect(jsonPath("$.errors", hasItem(hasEntry("field", "name"))))
    ;

  }
}

package springrest;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

public class SimpleJUnitTest {

  Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  @Test
  public void testmethod() {
    logger.info("Basic test");
    Assertions.assertTrue("Test Driven Development".length() > 0);
  }

  @BeforeEach
  @DisplayName("execute this method before every test method")
  public void beforeATestMethod() {
    logger.info("Before a test method");
  }

  @AfterEach
  @DisplayName("execute this method after every test method")
  public void afterATestMethod() {
    logger.info("after a test method");
  }


  @Test
  @DisplayName("Hamcrest  assertions mixed with Junit Assertions")
  void testHamcrestAssertions() {
    assertThat("Hello! World", isA(String.class));
    assertAll("person",
        () -> assertEquals("jane", "JaNe".toLowerCase()),
        () -> assertTrue(1 < 2, "1 should be less than 2")
    );

    assertThrows(ArithmeticException.class, () -> {
      int returnValue = 1 % 0;
    });
  }


  @Test
  @DisplayName("testing assumptions. value source is env property")
  void assumptionTest() {
    assumingThat("DEV".equals(System.getenv("ENV")),
        () -> logger.info("This group should run in Dev env")
    );

    assumeTrue(Boolean.valueOf(System.getProperty("assumptions")));
    assertTrue(Boolean.valueOf(System.getProperty("assumptions")), "This should be true");
  }


  @Test
  @EnabledIf({"systemProperty.get('assumptions').equals('true')", "1 > 2"})
  void givenEnabledIfExpression_WhenTrue_ThenTestExecuted() {
  }

  @RepeatedTest(value = 5, name = "Executing {currentRepetition} of {totalRepetitions}")
  void repeatedTest(RepetitionInfo repetitionInfo, TestInfo testInfo) {
    logger.info(">> " + repetitionInfo.getCurrentRepetition() + " - " + testInfo.getDisplayName());
  }

  @ParameterizedTest
  @ValueSource(ints = {1, 3, 5, -3, 15, Integer.MAX_VALUE})
    // six numbers
  void isOdd_ShouldReturnTrueForOddNumbers(int number) {
    logger.info(number + " >> ");
  }

  @ParameterizedTest
  @MethodSource("steamOfData")
  void paramterizedMethod(Integer value) {
    logger.info(value.toString());
  }


  static Stream<Integer> steamOfData() {
    return IntStream.range(1, 20).mapToObj(i -> i);
  }

}

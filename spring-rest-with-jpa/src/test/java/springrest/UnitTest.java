package springrest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UnitTest {

  Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  @Test
  public void testMyCode(TestInfo testInfo) {
    logger.info("This is before Test Method" + testInfo.getDisplayName());
    int bussionResult = 1;
    assertEquals(1, bussionResult, "Expecting a value 1 ");

    assertAll(
        () -> assertTrue(1 == 1),
        () -> assertFalse(1 > 2)
    );

  }

  private int calculateExchage(int rate) {
    String msg = String.format(" exchange rate for %s ", rate);
    logger.info(msg);
    return rate;
  }

//  @Test
//  @EnabledIf("systemProperty.get('test2') == 'true'")
//  @ParameterizedTest
//  @ValueSource(ints = {1, 2, 3})
//  void testMethod2(int rate) {
//    logger.info("Test method 2");
//    calculateExchage(rate);
//
//  }


  @Test
  @ParameterizedTest
  @MethodSource("exchangeRatesServiceList")
  void testMethod3(Integer rate) {
    logger.info("Test method 3");
    calculateExchage(rate);

  }

  static Stream<Integer> exchangeRatesServiceList() {
    return IntStream.range(1, 10).mapToObj(i -> i);
  }

  @BeforeEach
  void setupMyData() {
    logger.info("This is before Test Method");
  }
}

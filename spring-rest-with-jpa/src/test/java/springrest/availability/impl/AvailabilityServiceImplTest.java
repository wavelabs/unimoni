package springrest.availability.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;

import junit.framework.Assert;
import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import springrest.DateRange;
import springrest.availability.AvailabilityQuery;
import springrest.availability.AvailabilityStatus;
import springrest.booking.BookingService;
import springrest.inventory.InventoryService;
import springrest.model.Booking;
import springrest.model.Pricing;
import springrest.model.PricingModel;
import springrest.model.Room;
import springrest.model.RoomCategory;

@RunWith(JMockit.class)
public class AvailabilityServiceImplTest {

	@Tested
	private AvailabilityServiceImpl availabilityServiceImpl;

	@Injectable
	private BookingService bookingService;

	@Injectable
	private InventoryService inventoryService;

	private Date resultDate = new Date();

	private AvailabilityQuery getElseAvailabilityQuery() {
		AvailabilityQuery query = new AvailabilityQuery(getDateRange(), 1);
		return query;
	}

	private AvailabilityQuery getIfAvailabilityQuery() {
		AvailabilityQuery query = new AvailabilityQuery(getDateRange(), 0);
		return query;
	}

	private DateRange getDateRange() {
		Calendar cal = Calendar.getInstance();
		cal.set(2019, 02, 24);
		Date from = cal.getTime();
		cal.set(2019,02, 29);
		Date until = cal.getTime();
		DateRange daterange = new DateRange(from, until);
		return daterange;
	}

	private Booking getBooking() {
		Booking booking = new Booking();
		booking.setCategoryId(1);
		booking.setId(100);
		booking.setCreatedAt(new Date());
		booking.setCustomerName("aaa");
		booking.setFrom(new Date());
		booking.setRoomId(1);
		booking.setUntil(new Date());
		booking.setUpdatedAt(resultDate);
		return booking;
	}

	private RoomCategory getCategory() {
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setDescription("aaaaa");
		roomCategory.setName("sdf");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		roomCategory.setPricing(pricing);
		return roomCategory;
	}
	//testcase for getLastUpdatedBooking method if qurey object is null
	@Test(expected=IllegalArgumentException.class)
	public void testGetLastUpdatedBooking1() {
		AvailabilityQuery query=null;
		availabilityServiceImpl.getLastUpdatedBooking(query);
		assertEquals(null, query);
	}
	//testcase for getLastUpdatedBooking when categoryid=0
	@Test
	public void testGetLastUpdatedBooking2() {
		Booking booking = getBooking();
		RoomCategory cate = getCategory();
		AvailabilityQuery query = getElseAvailabilityQuery();
		//we are mocking inventoryService and bookingService
		new Expectations() {
			{
				inventoryService.getRoomCategory(query.getCategoryId());
				returns(cate);
				times = 1;
				bookingService.getLastUpdatedBooking(query.getDateRange(), cate);
				returns(booking);
				times = 1;
			}
		};
		//checking tested class method
		Date date = availabilityServiceImpl.getLastUpdatedBooking(query);
		//we are asserting updatedbooking date with expected and actual value
		new Verifications() 
		{{
			assertEquals(resultDate, date);
		}};

	}
	//testcase for getLastUpdatedBooking when categoryid>0
	@Test
	public void testGetLastUpdatedBooking3() {
		Booking booking = getBooking();
		AvailabilityQuery query = getIfAvailabilityQuery();
		//we are mocking bookingservice & calling the method in that by passing daterange
		new Expectations() {
			{
				bookingService.getLastUpdatedBooking(query.getDateRange());
				returns(booking);
				times = 1;
			}
		};
		//checking tested class method
		Date date = availabilityServiceImpl.getLastUpdatedBooking(query);
		//we are asserting updatedbooking date with expected and actual value
		new Verifications() 
		{{
			assertEquals(resultDate, date);
		}};

	}

	private List<Booking> getBookingsList() {
		Booking booking1 = new Booking();
		Calendar fromCal = Calendar.getInstance();
		fromCal.set(2019, 02, 24);
		Calendar toCal = Calendar.getInstance();
		toCal.set(2019, 02,29);
		booking1.setId(101);
		booking1.setCategoryId(1);
		booking1.setCreatedAt(new Date());
		booking1.setCustomerName("lakshmi");
		booking1.setFrom(fromCal.getTime());
		booking1.setRoomId(1);
		booking1.setUntil(toCal.getTime());
		booking1.setUpdatedAt(resultDate);
		Booking booking2 = new Booking();
		booking2.setId(102);
		booking2.setCategoryId(0);
		booking2.setCreatedAt(new Date());
		booking2.setCustomerName("ramya");
		booking2.setFrom(fromCal.getTime());
		booking2.setRoomId(2);
		booking2.setUntil(toCal.getTime());
		booking2.setUpdatedAt(resultDate);
		Booking booking3 = new Booking();
		booking3.setId(103);
		booking3.setCategoryId(2);
		booking3.setCreatedAt(new Date());
		booking3.setCustomerName("lokesh");
		booking3.setFrom(fromCal.getTime());
		booking3.setRoomId(3);
		booking3.setUntil(toCal.getTime());
		booking3.setUpdatedAt(resultDate);
		List<Booking> bookings = new ArrayList<Booking>();
		bookings.add(booking1);
		bookings.add(booking2);
		bookings.add(booking3);
		return bookings;
	}

	private List<Room> getAllRooms() {
		Room room1 = new Room();
		room1.setId(1);
		room1.setName("101");
		room1.setDescription("ac");
		RoomCategory category1 = getCategory();
		room1.setRoomCategory(category1);
		Room room2 = new Room();
		room2.setId(2);
		room2.setName("102");
		room2.setDescription("non-ac");
		room2.setRoomCategory(category1);
		Room room3 = new Room();
		room3.setId(3);
		room3.setName("103");
		room3.setDescription("ac");
		room3.setRoomCategory(category1);
		Room room4 = new Room();
		room4.setId(4);
		room4.setName("104");
		room4.setDescription("non-ac");
		room4.setRoomCategory(category1);
		Room room5 = new Room();
		room5.setId(5);
		room5.setName("105");
		room5.setDescription("ac");
		room5.setRoomCategory(category1);
		List<Room> rooms = new ArrayList<Room>();
		rooms.add(room1);
		rooms.add(room2);
		rooms.add(room3);
		rooms.add(room4);
		rooms.add(room5);
		return rooms;
	}
	//testcase for getAvailableRooms if query=null
	@Test(expected=IllegalArgumentException.class)
	public void testGetAvailableRooms1() {
		AvailabilityQuery query=null;
		availabilityServiceImpl.getAvailableRooms(query);
		assertEquals(null, query);
	}
	//testcase for getAvailableRooms when categoryid=0
	@Test
	public void testGetAvailableRooms2() {
		List<Booking> bookings = getBookingsList();
		List<Room> allRooms = getAllRooms();
		AvailabilityQuery query = getIfAvailabilityQuery();
		/*
		 * we are mocking bookingservice and calling the method in that by passing daterange as parameter which returns bookings 
		 * we are mocking inventoryservice and calling the method in that which returns allrooms
		 */
		new Expectations() {
			{
				bookingService.getBookings(query.getDateRange());
				returns(bookings);
				times = 1;
				inventoryService.getAllRooms();
				returns(allRooms);
				times = 1;
			}
		};
		//calling testclass method
		List<AvailabilityStatus> statuses = availabilityServiceImpl.getAvailableRooms(query);
		for(AvailabilityStatus status : statuses) {
			if(status.getDate().getDate()==25) {
				assertEquals(2, status.getRooms().size());//asserting available rooms with expected and actual values
				break;
			}
		}
	}
	//test case for getAvailableRooms when categoryid >= 0
	@Test
	public void testGetAvailableRooms3() {
		AvailabilityQuery query = getElseAvailabilityQuery();
		List<Booking> bookings = getBookingsList();
		RoomCategory category = getCategory();
		List<Room> allRooms = getAllRooms();
		/*  
		 * we are mocking inventoryservice by using @Injectable & calling the method in that by passing categoryid which returns category.
		   we are mocking inventoryservice by using @Injectable & calling the method in that by passing category which returns allrooms.
		   we are mocking bookingService by using @Injectable & calling the method in that by passing daterange and category which returns bookings.
		 *
		 */
		new Expectations() {
			{
				inventoryService.getRoomCategory(query.getCategoryId());
				returns(category);
				times = 1;
				inventoryService.getAllRoomsWithCategory(category);
				returns(allRooms);
				times = 1;
				bookingService.getBookings(query.getDateRange(), category);
				returns(bookings);
				times = 1;			
			}
		};
		//calling tested class method by using which retuns statuses.
		List<AvailabilityStatus> statuses = availabilityServiceImpl.getAvailableRooms(query);
		for(AvailabilityStatus status : statuses) {
			if(status.getDate().getDate()==25) {
				assertEquals(2, status.getRooms().size());//asserting availablerooms with expected and actual values
				break;
			}
		} 
	}
	//testcase for getBookedRoomsOn which is a private method.
	@Test
	public void testGetBookedRoomsOn() {
		List<Booking> bookings = getBookingsList();
		Calendar cal = Calendar.getInstance();
		cal.set(2019, 02, 22);
		Date date=cal.getTime();
		Set<Long> roomids = Deencapsulation.invoke(availabilityServiceImpl, "getBookedRoomsOn", bookings, date);
		System.out.println("roomids" + roomids);
		assertEquals(0, roomids.size());
	}
	//testcase for toRoomList  which is a private method
	@Test
	public void testToRoomList() {
		List<Booking> bookings = getBookingsList();
		AvailabilityServiceImplTest availabilityServiceImplTest=new AvailabilityServiceImplTest();
		Date fromDate=availabilityServiceImplTest.getDateRange().getFrom();
		List<Room> allRooms = getAllRooms();
		Set<Long> roomids = Deencapsulation.invoke(availabilityServiceImpl, "getBookedRoomsOn", bookings,fromDate);
		List<Room> rooms = Deencapsulation.invoke(availabilityServiceImpl, "toRoomList", allRooms, roomids);
		System.out.println(rooms);
		assertEquals(5, rooms.size());
	}
}

package springrest;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import springrest.inventory.InventoryService;
import springrest.model.Pricing;
import springrest.model.PricingModel;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.repo.RoomRepository;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MockVsSpyTest {

  @Mock
  InventoryService mockInventoryService;

  @SpyBean
  InventoryService spyInventoryService;

  static final Logger logger = LoggerFactory.getLogger(MockVsSpyTest.class);

  @Autowired
  RoomRepository roomRepository;


  @Test
  void testMockAndSpy() {
    roomRepository.save(new Room()
        .setName("A/C Room")
        .setDescription("Suite")
        .setRoomCategory(new RoomCategory()
            .setDescription("VIP Suite")
            .setName("VIP-S")
            .setPricing(new Pricing(PricingModel.FIXED)
                .setPriceGuest1(100.00)
            )
        )
    );

    List<Room> roomsByMock = mockInventoryService.getAllRooms();
    logger.info(mockInventoryService.getClass().getName() + " Mock Service Rooms: " + roomsByMock.size());


    List<Room> roomsBySpy = spyInventoryService.getAllRooms();
    logger.info(mockInventoryService.getClass().getName() + "Spy Service Rooms:" + roomsBySpy.size());

  }

}

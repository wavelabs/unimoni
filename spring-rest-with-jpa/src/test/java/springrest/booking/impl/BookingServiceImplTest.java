package springrest.booking.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.springframework.data.domain.Sort;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import springrest.DateRange;
import springrest.RecordNotFoundException;
import springrest.booking.BookingRequest;
import springrest.booking.BookingResponse;
import springrest.booking.BookingResponse.Status;
import springrest.booking.CreditCardDetails;
import springrest.inventory.InventoryService;
import springrest.model.Booking;
import springrest.model.Pricing;
import springrest.model.PricingModel;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.repo.BookingRepository;
import springrest.repo.RoomRepository;

public class BookingServiceImplTest {

	@Tested
	private BookingServiceImpl bookingServiceImpl;
	@Injectable
	private InventoryService inventoryService;
	@Injectable
	private BookingRepository bookingRepository;

	Date resultDate=new Date();
	private DateRange getDateRange() {
		Calendar calendar=Calendar.getInstance();
		calendar.set(2019,02,25);
		Date fromDate=calendar.getTime();
		calendar.set(2019,02,29);
		Date untilDate=calendar.getTime();
		DateRange dateRange=new DateRange(fromDate,untilDate);
		return dateRange;
	}
	private CreditCardDetails getCreditCardDetails() {
		CreditCardDetails creditcardDetails=new CreditCardDetails("hari","1235347342893","july 2024","234");
		return creditcardDetails;
	}
	private BookingRequest getBookingRequest() {
		DateRange dateRange=getDateRange();
		CreditCardDetails creditCardDetails=getCreditCardDetails();
		BookingRequest bookingRequest=new BookingRequest(1l,dateRange,"hari",creditCardDetails);
		return bookingRequest;
	}
	private Room getRoom() {
		Room room=new Room();
		room.setId(1);
		room.setName("102");
		room.setDescription("non ac");
		RoomCategory roomCategory = new RoomCategory();
		roomCategory.setId(1);
		roomCategory.setName("single");
		roomCategory.setDescription("only one person can stay");
		Pricing pricing = new Pricing(PricingModel.FIXED);
		pricing.setId(1);
		pricing.setPriceGuest1(5000.00);
		pricing.setPriceGuest2(7000.00);
		pricing.setPriceGuest3(6000.00);
		roomCategory.setPricing(pricing);
		room.setRoomCategory(roomCategory);
		return room;
	}
	private List<Booking> getBookingsList() {
		Booking booking1 = new Booking();
		Calendar fromCal = Calendar.getInstance();
		fromCal.set(2019, 02, 24);
		Calendar toCal = Calendar.getInstance();
		toCal.set(2019, 02,29);
		booking1.setId(101);
		booking1.setCategoryId(1);
		booking1.setCreatedAt(new Date());
		booking1.setCustomerName("lakshmi");
		booking1.setFrom(fromCal.getTime());
		booking1.setRoomId(1);
		booking1.setUntil(toCal.getTime());
		booking1.setUpdatedAt(resultDate);
		Booking booking2 = new Booking();
		booking2.setId(102);
		booking2.setCategoryId(0);
		booking2.setCreatedAt(new Date());
		booking2.setCustomerName("ramya");
		booking2.setFrom(fromCal.getTime());
		booking2.setRoomId(2);
		booking2.setUntil(toCal.getTime());
		booking2.setUpdatedAt(resultDate);
		Booking booking3 = new Booking();
		booking3.setId(103);
		booking3.setCategoryId(2);
		booking3.setCreatedAt(new Date());
		booking3.setCustomerName("lokesh");
		booking3.setFrom(fromCal.getTime());
		booking3.setRoomId(3);
		booking3.setUntil(toCal.getTime());
		booking3.setUpdatedAt(resultDate);
		List<Booking> bookings = new ArrayList<Booking>();
		bookings.add(booking1);
		bookings.add(booking2);
		bookings.add(booking3);
		return bookings;
	}
	private RoomCategory getRoomCategory() {
		RoomCategory roomCategory1 = new RoomCategory();
		roomCategory1.setId(1);
		roomCategory1.setName("Single");
		roomCategory1.setDescription("only one person can stay");
		Pricing pricing1=new Pricing(PricingModel.FIXED);
		pricing1.setId(1);
		pricing1.setPriceGuest1(5000.00);
		pricing1.setPriceGuest2(6000.00);
		pricing1.setPriceGuest3(8000.00);
		roomCategory1.setPricing(pricing1);
		return roomCategory1;
	}
	private Booking getBooking() {
		Booking booking = new Booking();
		Calendar fromCal = Calendar.getInstance();
		fromCal.set(2019, 02, 24);
		Calendar toCal = Calendar.getInstance();
		toCal.set(2019, 02,29);
		booking.setId(101);
		booking.setCategoryId(1);
		booking.setCreatedAt(new Date());
		booking.setCustomerName("lakshmi");
		booking.setFrom(fromCal.getTime());
		booking.setRoomId(1);
		booking.setUntil(toCal.getTime());
		booking.setUpdatedAt(resultDate);
		return booking;
	}
	@Test
	public void testBook() {

		BookingRequest request=getBookingRequest();
		Room room = getRoom();
		Booking booking=new Booking();
		booking.setId(1);
		booking.setRoomId(room.getId());
		booking.setCategoryId(room.getRoomCategory().getId());
		booking.setFrom(request.getDateRange().getFrom());
		booking.setUntil(request.getDateRange().getUntil());
		booking.setCustomerName(request.getCustomerName());

		new Expectations() {{
			inventoryService.getRoom(request.getRoomId());
			returns(room);
			times=1;
		}};
		BookingResponse bookingResponse=new BookingResponse(Status.BOOKED,booking.getId());
		BookingResponse bookResponse=bookingServiceImpl.book(request);
		new Verifications() {{
			assertEquals(Status.BOOKED,bookResponse.getStatus());
		}};	
	}
	@Test(expected = IllegalArgumentException.class)
	 public void testGetBooking1() {
		Booking booking = new Booking();
		booking.setId(0);
		booking.setCustomerName("sowmya");
		Booking bookingObj = bookingServiceImpl.getBooking(booking.getId());
		assertEquals(0,booking.getId());
	 }
	@Test
	public void testGetBooking2() {
		Booking booking = getBooking();
		Optional<Booking> optional=Optional.of(booking);
		new Expectations() {{
			bookingRepository.findById(booking.getId());
			returns(optional);
			times=1;
		}};
		Booking bookingObj=bookingServiceImpl.getBooking(booking.getId());
		new Verifications() {{
			assertEquals("lakshmi", booking.getCustomerName());
		}};
	}
	@Test(expected=RecordNotFoundException.class)
	public void testGetBooking3() {
		Booking booking = getBooking();
		Optional<Booking> optional=Optional.empty();
		new Expectations() {{
			bookingRepository.findById(anyLong);
			returns(optional);
			times=1;
		}};
		Booking bookingObj=bookingServiceImpl.getBooking(1l);
		assertEquals(null, bookingObj);
		}
	//testcase for getBookings(dateRange)
	@Test
	public void testGetBookings1() {
		DateRange dateRange=getDateRange();
		List<Booking> bookings=getBookingsList();
		new Expectations() {{
			bookingRepository.findAllByFromAfterAndUntilBefore(dateRange.getFrom(), dateRange.getUntil());
			returns(bookings);
			times=1;
		}};
		List<Booking> bookingList=bookingServiceImpl.getBookings(dateRange);
		new Verifications() {{
			assertEquals(3, bookingList.size());
		}};
	}
	//testcase for getBookings(dateRange,Roomcategory)
	@Test
	public void testGetBookings2() {
		DateRange dateRange=getDateRange();
		List<Booking> bookings=getBookingsList();
		RoomCategory roomCategory=getRoomCategory();
		new Expectations() {{
			bookingRepository.findAllByFromAfterAndUntilBeforeAndCategoryId(dateRange.getFrom(), dateRange.getUntil(), roomCategory.getId());
			returns(bookings);
			times=1;
		}};
		List<Booking> bookingList=bookingServiceImpl.getBookings(dateRange, roomCategory);
		new Verifications() {{
			assertEquals(3, bookingList.size());
		}};
	}
	//testcase for getLastUpdatedBooking(DateRange dateRange)
	@Test
	public void testGetLastUpdatedBooking1() {
		DateRange dateRange=getDateRange();
		Sort sort = Sort.by(Sort.Direction.DESC, "updatedAt");
		Booking booking=getBooking();
		new Expectations() {{
			bookingRepository.findOneByFromAfterAndUntilBefore(dateRange.getFrom(), dateRange.getUntil(), sort);
			returns(booking);
			times=1;
		}};
		Booking bookingObj=bookingServiceImpl.getLastUpdatedBooking(dateRange);
		new Verifications() {{
			assertEquals("lakshmi", bookingObj.getCustomerName());
		}};
	}
	//testcase for getLastUpdatedBooking(DateRange dateRange, RoomCategory category)
	@Test
	public void testGetLastUpdatedBooking2() {
		DateRange dateRange=getDateRange();
		Sort sort = Sort.by(Sort.Direction.DESC, "updatedAt");
		RoomCategory roomCategory=getRoomCategory();
		Booking booking=getBooking();
		new Expectations() {{
			bookingRepository.findOneByFromAfterAndUntilBeforeAndCategoryId(dateRange.getFrom(), dateRange.getUntil(), booking.getCategoryId(), sort);
			returns(booking);
			times=1;
		}};
		Booking bookingObj=bookingServiceImpl.getLastUpdatedBooking(dateRange, roomCategory);
		new Verifications() {{
			assertEquals("lakshmi", bookingObj.getCustomerName());
		}};
	}
	

}

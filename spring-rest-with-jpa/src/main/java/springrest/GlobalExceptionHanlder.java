package springrest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import springrest.web.ApiResponse;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHanlder
//    extends ResponseEntityExceptionHandler
{

  @ExceptionHandler(RecordNotFoundException.class)
  public ResponseEntity<?> handleRecordNotFoundException(RecordNotFoundException exception, WebRequest request) {
    return new ResponseEntity(
        new ApiResponse(ApiResponse.Status.ERROR, null, new ApiResponse.ApiError(100, exception.getMessage())),
        HttpStatus.OK);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<?> handleValidationErrors(MethodArgumentNotValidException invalidRequest, WebRequest request) {
    List<ApiResponse.ApiError> errors = invalidRequest.getBindingResult().getFieldErrors().stream()
        .map(e -> new springrest.web.ApiResponse.ApiError(400, e.getDefaultMessage()).setField(e.getField()))
        .collect(Collectors.toList());
    return new ResponseEntity(new ApiResponse(ApiResponse.Status.ERROR, null, null).setErrors(errors),
        HttpStatus.BAD_REQUEST);
  }
}

package springrest.availability;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import springrest.model.Room;

import java.util.Date;
import java.util.List;

/**
 * Captures the availability status for a day.
 */
public class AvailabilityStatus {

  private final Date date;
  private final List<Room> rooms;

  public AvailabilityStatus(Date date, List<Room> rooms) {
    this.date = date;
    this.rooms = rooms;
  }

  public Date getDate() {
    return date;
  }

  public List<Room> getRooms() {
    return rooms;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }
}

package springrest.availability;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import springrest.DateRange;

/**
 * Encapsulates search criteria for room availability.
 */
public class AvailabilityQuery {

  private final DateRange dateRange;
  private final long categoryId;

  public AvailabilityQuery(DateRange dateRange, long categoryId) {
    if (dateRange == null) {
      throw new IllegalArgumentException("dateRange is mandatory");
    }
    this.dateRange = dateRange;
    this.categoryId = categoryId;
  }

  public DateRange getDateRange() {
    return dateRange;
  }

  public long getCategoryId() {
    return categoryId;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }
}

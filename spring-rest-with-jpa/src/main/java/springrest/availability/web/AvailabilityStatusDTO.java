package springrest.availability.web;

import springrest.inventory.web.RoomDTO;
import springrest.model.Room;

import java.util.List;
import java.util.stream.Collectors;

/**
 */
public class AvailabilityStatusDTO {

  private final String date;
  private final List<RoomDTO> rooms;

  public AvailabilityStatusDTO(String date, List<Room> rooms) {
    this.date = date;
    this.rooms = rooms
        .stream()
        .map(RoomDTO::new)
        .collect(Collectors.toList());
  }

  public String getDate() {
    return date;
  }

  public List<RoomDTO> getRooms() {
    return rooms;
  }
}

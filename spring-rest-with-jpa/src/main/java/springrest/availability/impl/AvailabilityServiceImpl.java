package springrest.availability.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import springrest.availability.AvailabilityQuery;
import springrest.availability.AvailabilityService;
import springrest.availability.AvailabilityStatus;
import springrest.booking.BookingService;
import springrest.inventory.InventoryService;
import springrest.model.Booking;
import springrest.model.Room;
import springrest.model.RoomCategory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link springrest.availability.AvailabilityService} implementation.
 */
@Component
public class AvailabilityServiceImpl implements AvailabilityService {

  private static final int ONE_DAY = 24 * 60 * 60 * 1000;

  @Autowired
  private InventoryService inventoryService;
  @Autowired
  private BookingService bookingService;
  
  @Override
  public Date getLastUpdatedBooking(AvailabilityQuery query) {
    if (query == null) {
      throw new IllegalArgumentException("query cannot be null");
    }
    Booking booking;
    if (query.getCategoryId() <= 0) {
      booking = bookingService.getLastUpdatedBooking(query.getDateRange());
    } else {
      RoomCategory category = inventoryService.getRoomCategory(query.getCategoryId());
      booking = bookingService.getLastUpdatedBooking(query.getDateRange(), category);
    }
    return booking == null ? null : booking.getUpdatedAt();
  }

  @Override
  public List<AvailabilityStatus> getAvailableRooms(AvailabilityQuery query) {
    if (query == null) {
      throw new IllegalArgumentException("query cannot be null");
    }
    // we look up bookings and rooms
    List<Booking> bookings;
    List<Room> allRooms;
    if (query.getCategoryId() <= 0) {
      bookings = bookingService.getBookings(query.getDateRange());
      allRooms = inventoryService.getAllRooms();
    } else {
      RoomCategory category = inventoryService.getRoomCategory(query.getCategoryId());
      allRooms = inventoryService.getAllRoomsWithCategory(category);
      bookings = bookingService.getBookings(query.getDateRange(), category);
    }
    // and split statuses by day
    List<AvailabilityStatus> statuses = new ArrayList<>();          
    Date date = query.getDateRange().getFrom();
    System.out.println(date);
    System.out.println(query.getDateRange().getUntil());
    while (date.getTime() <= query.getDateRange().getUntil().getTime()) {
    	Set<Long> ids= getBookedRoomsOn(bookings, date);
    	System.out.println(ids);
    	List<Room> rooms = toRoomList(allRooms, ids);
    	System.out.println(rooms);
       statuses.add(new AvailabilityStatus(date, rooms));
      date = new Date(date.getTime() + ONE_DAY);
    }
    return statuses;
  }

  private Set<Long> getBookedRoomsOn(List<Booking> bookings, Date date) {
    Set<Long> roomIds = new HashSet<>();
    long time = date.getTime();
    System.out.println("testing");
    roomIds.addAll(bookings.stream()
        .filter(booking -> booking.getFrom().getTime() <= time && booking.getUntil().getTime() >= date.getTime())
        .map(Booking::getRoomId)
        .collect(Collectors.toList()));
    return roomIds;
  }

  private List<Room> toRoomList(List<Room> allRooms, Set<Long> bookedRoomIds) {
    return allRooms
        .stream()
        .filter(room -> !bookedRoomIds.contains(room.getId()))
        .collect(Collectors.toList());
  }
}

package springrest.inventory.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springrest.RecordNotFoundException;
import springrest.inventory.InventoryService;
import springrest.model.Pricing;
import springrest.model.PricingModel;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.repo.RoomCategoryRepository;
import springrest.repo.RoomRepository;

import java.util.List;
import java.util.Optional;

/**
 * {@link springrest.inventory.InventoryService} implementation.
 */
@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {

  private static final Logger LOGGER = LoggerFactory.getLogger(InventoryServiceImpl.class);

  @Autowired
  RoomRepository roomRepository;

  @Autowired
  RoomCategoryRepository roomCategoryRepository;

  public InventoryServiceImpl() {
  }

  @Override
  public void addRoomCategory(RoomCategory category) {
    if (category.getId() > 0) {
      throw new IllegalArgumentException("category already exists");
    }
    checkPricing(category);
    roomCategoryRepository.save(category);
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Added new room category {}", category);
    }
  }

  @Override
  public void updateRoomCategory(RoomCategory category) {
    if (category.getId() == 0) {
      throw new IllegalArgumentException("room category does not exist");
    }
    checkPricing(category);
    roomCategoryRepository.save(category);
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Updated room category {}", category);
    }
  }

  /**
   * Validates that the pricing object is valid.
   *
   * @param category the category to check the pricing for
   */
  private void checkPricing(RoomCategory category) {
    Pricing pricing = category.getPricing();
    if (pricing == null) {
      throw new IllegalArgumentException("category must have a pricing");
    }
    if (pricing.getPriceGuest1() == null) {
      throw new IllegalArgumentException("Pricing requires a guest 1 price");
    }
    if (pricing.getPricingModel() == PricingModel.SLIDING && pricing.getPriceGuest2() == null) {
      throw new IllegalArgumentException("Sliding pricing requires a guest 2 price");
    }
  }

  @Override
  public RoomCategory getRoomCategory(long categoryId) {
    if (categoryId <= 0) {
      throw new IllegalArgumentException("Invalid category ID. It must be greater than zero");
    }
    Optional<RoomCategory> category = roomCategoryRepository.findById(categoryId);
    if (!category.isPresent()) {
      throw new RecordNotFoundException("No room category with ID " + categoryId);
    }
    return category.get();
  }

  @Override
  public void deleteRoomCategory(long categoryId) {
    if (categoryId <= 0) {
      throw new IllegalArgumentException("Invalid category ID. It must be greater than zero");
    }
    RoomCategory category = getRoomCategory(categoryId);
    roomCategoryRepository.delete(category);
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Deleted room category {}", category);
    }
  }

  @Override
  public List<RoomCategory> getAllRoomCategories() {
    return roomCategoryRepository.findAll();
  }

  public void addRoom(Room room) {
    if (room.getId() > 0) {
      throw new IllegalArgumentException("room already exists");
    }
    roomRepository.save(room);
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Added new room {}", room);
    }
  }

  @Transactional(readOnly = true)
  public Room getRoom(long roomId) {
    if (roomId <= 0) {
      throw new IllegalArgumentException("Invalid room ID. It must be greater than zero");
    }
    Optional<Room> room = roomRepository.findById(roomId);
    if (!room.isPresent()) {
      throw new RecordNotFoundException("No room with ID " + roomId);
    }
    return room.get();
  }

  public void updateRoom(Room room) {
    if (room.getId() == 0) {
      throw new IllegalArgumentException("room does not exist");
    }
    roomRepository.save(room);
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Updated room {}", room);
    }
  }

  public void deleteRoom(long roomId) {
    if (roomId <= 0) {
      throw new IllegalArgumentException("Invalid room ID. It must be greater than zero");
    }
    Room room = getRoom(roomId);
    roomRepository.delete(room);
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Deleted room {}", room);
    }
  }

  @Transactional(readOnly = true)
  public List<Room> getAllRooms() {
    return roomRepository.findAll();
  }

  @Transactional(readOnly = true)
  public List<Room> getAllRoomsWithCategory(RoomCategory category) {
    Room r = new Room();
    r.setRoomCategory(category);
    Example<Room> matchingRooms = Example.of(r);
    return roomRepository.findAll(matchingRooms);
  }
}

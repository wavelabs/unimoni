package springrest.inventory.web;

import springrest.model.Room;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class RoomDTO implements Serializable {

  private static final long serialVersionUID = 2682046985632747474L;

  private long id;

  @NotBlank
  @Size(min = 3, max =20, message = "{room.name.length}")
  private String name;
  private long roomCategoryId;
  private String description;

  public RoomDTO setId(long id) {
    this.id = id;
    return this;
  }

  public RoomDTO setName(String name) {
    this.name = name;
    return this;
  }

  public RoomDTO setRoomCategoryId(long roomCategoryId) {
    this.roomCategoryId = roomCategoryId;
    return this;
  }

  public RoomDTO setDescription(String description) {
    this.description = description;
    return this;
  }

  public RoomDTO() {
  }

  public RoomDTO(Room room) {
    this.id = room.getId();
    this.name = room.getName();
    this.roomCategoryId = room.getRoomCategory().getId();
    this.description = room.getDescription();
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public long getRoomCategoryId() {
    return roomCategoryId;
  }

  public String getDescription() {
    return description;
  }
}

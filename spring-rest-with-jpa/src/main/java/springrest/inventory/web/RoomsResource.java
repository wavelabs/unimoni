package springrest.inventory.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import springrest.RecordNotFoundException;
import springrest.inventory.InventoryService;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.web.ApiResponse;
import springrest.web.ApiResponse.ApiError;
import springrest.web.ApiResponse.Status;
import springrest.web.ListApiResponse;

import javax.validation.Valid;
import java.util.stream.Collectors;

/**
 * Resource (controller) class for {@link Room}s.
 */
@RestController
@RequestMapping("/rooms")
public class RoomsResource {

  @Autowired
  private InventoryService inventoryService;

  public RoomsResource() {

  }

  public RoomsResource(InventoryService service) {
    this.inventoryService = service;
  }

  @RequestMapping(value = "/{roomId}", method = RequestMethod.GET)
  public ApiResponse getRoom(@PathVariable("roomId") long id) {
    try {
      Room room = inventoryService.getRoom(id);
      return new ApiResponse(ApiResponse.Status.OK, new RoomDTO(room));
    } catch (RecordNotFoundException e) {
      return new ApiResponse(ApiResponse.Status.ERROR, null, new ApiResponse.ApiError(999, "No room with ID " + id));
    }
  }

  @RequestMapping(value = "/desc/{roomId}", method = RequestMethod.GET)
  public ApiResponse getRoom2(@PathVariable("roomId") long id) {
      Room room = inventoryService.getRoom(id);
      return new ApiResponse(ApiResponse.Status.OK, new RoomDTO(room));
  }

  @RequestMapping(method = RequestMethod.POST)
  public ApiResponse addRoom(@Valid @RequestBody RoomDTO room) {
    Room newRoom = createRoom(room);
    return new ApiResponse(Status.OK, new RoomDTO(newRoom), null);
  }

  private Room createRoom(RoomDTO room) {
    return createRoom(room.getName(), room.getDescription(), room.getRoomCategoryId());
  }

  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public ApiResponse addRoom(String name, String description, long roomCategoryId) {
    Room room = createRoom(name, description, roomCategoryId);
    return new ApiResponse(Status.OK, new RoomDTO(room));

  }

  private Room createRoom(String name, String description, long roomCategoryId) {
    Room room = new Room();
    room.setName(name);
    room.setDescription(description);
    RoomCategory category = inventoryService.getRoomCategory(roomCategoryId);
    room.setRoomCategory(category);
    inventoryService.addRoom(room);
    return room;
  }

  @RequestMapping(value = "/{roomId}", method = RequestMethod.PUT)
  public ApiResponse updateRoom(@PathVariable("roomId") long id, @RequestBody RoomDTO updatedRoom) {
    try {
      Room room = inventoryService.getRoom(id);
      updateRoom(updatedRoom, room);
      return new ApiResponse(Status.OK, new RoomDTO(room));
    } catch (RecordNotFoundException e) {
      return new ApiResponse(Status.ERROR, null, new ApiError(999, "No room with ID " + id));
    }
  }

  @RequestMapping(value = "/{roomId}", method = RequestMethod.POST, headers = {"X-HTTP-Method-Override=PUT"})
  public ApiResponse updateRoomAsPost(@PathVariable("roomId") long id, @RequestBody RoomDTO updatedRoom) {
    return updateRoom(id, updatedRoom);
  }

  private void updateRoom(RoomDTO updatedRoom, Room room) {
    room.setName(updatedRoom.getName());
    room.setDescription(updatedRoom.getDescription());
    room.setRoomCategory(inventoryService.getRoomCategory(updatedRoom.getRoomCategoryId()));
    inventoryService.updateRoom(room);
  }

  @RequestMapping(value = "/{roomId}", method = RequestMethod.DELETE)
  public ApiResponse deleteRoom(@PathVariable("roomId") long id) {
    try {
      Room room = inventoryService.getRoom(id);
      inventoryService.deleteRoom(room.getId());
      return new ApiResponse(Status.OK, null);
    } catch (RecordNotFoundException e) {
      return new ApiResponse(Status.ERROR, null, new ApiError(999, "No room with ID " + id));
    }
  }

  @RequestMapping(method = RequestMethod.GET)
  public ListApiResponse getRoomsInCategory(@RequestParam("categoryId") long categoryId) {
    RoomCategory category = inventoryService.getRoomCategory(categoryId);
    return new ListApiResponse(Status.OK, inventoryService.getAllRoomsWithCategory(category)
        .stream().map(RoomDTO::new).collect(Collectors.toList()), null, 2,
        "http://localhost:8080/rooms?categoryId=" + categoryId + "&page=3", 13);
  }


  // controller level exception handler
  //@ExceptionHandler(RecordNotFoundException.class)
  public ResponseEntity<?> handleRecordNotFoundException(RecordNotFoundException exception, WebRequest request) {
    return new ResponseEntity(
        new ApiResponse(Status.ERROR, null, new ApiResponse.ApiError(400, exception.getMessage())),
        HttpStatus.FORBIDDEN);
  }
}

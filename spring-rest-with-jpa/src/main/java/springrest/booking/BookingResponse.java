package springrest.booking;

/**
 * This class captures the result of processing a booking request.
 */
public class BookingResponse {

  private final Status status;
  private long bookingId;
  public BookingResponse(Status status, long bookingId) {
    this.status = status;
    this.bookingId = bookingId;
  }

  public Status getStatus() {
    return status;
  }

  public long getBookingId() {
    return bookingId;
  }

  public static enum Status {
    BOOKED,
    REJECTED
  }
}

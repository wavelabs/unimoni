package springrest.booking.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springrest.RecordNotFoundException;
import springrest.booking.BookingRequest;
import springrest.booking.BookingResponse;
import springrest.booking.BookingService;
import springrest.web.ApiResponse;
import springrest.web.ApiResponse.ApiError;
import springrest.web.ApiResponse.Status;

import javax.validation.Valid;

/**
 *
 */
@RestController
@RequestMapping("/bookings")
public class BookingsResource {

	@Autowired
	private BookingService bookingService;

	public BookingsResource() {
	}

	@RequestMapping(method = RequestMethod.POST)
	public ApiResponse book(@Valid @RequestBody BookingRequest request) {
		BookingResponse response = bookingService.book(request);
		return new ApiResponse(ApiResponse.Status.OK, response);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/{bookingId}", method = RequestMethod.GET)
	public ApiResponse getBooking(@PathVariable("bookingId") long bookingId) {
		try {
			return new ApiResponse(Status.OK, new BookingDTO(bookingService.getBooking(bookingId)));
		} catch (RecordNotFoundException e) {
			return new ApiResponse(Status.ERROR, null, new ApiError(999, "No booking with ID " + bookingId));
		}
	}
}

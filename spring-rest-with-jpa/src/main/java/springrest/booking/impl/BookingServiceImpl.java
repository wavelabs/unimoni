package springrest.booking.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import springrest.DateRange;
import springrest.RecordNotFoundException;
import springrest.booking.BookingRequest;
import springrest.booking.BookingResponse;
import springrest.booking.BookingService;
import springrest.inventory.InventoryService;
import springrest.model.Booking;
import springrest.model.Room;
import springrest.model.RoomCategory;
import springrest.repo.BookingRepository;

import java.util.List;
import java.util.Optional;

/**
 * {@link BookingService} implementation.
 */
@Component
@Transactional
public class BookingServiceImpl implements BookingService {

  @Autowired
  private InventoryService inventoryService;

  @Autowired
  private BookingRepository bookingRepository;

  @Override
  @Transactional
  public BookingResponse book(BookingRequest request) {
    Booking booking = new Booking();
    Room room = inventoryService.getRoom(request.getRoomId());
    booking.setRoomId(room.getId());
    booking.setCategoryId(room.getRoomCategory().getId());
    booking.setFrom(request.getDateRange().getFrom());
    booking.setUntil(request.getDateRange().getUntil());
    booking.setCustomerName(request.getCustomerName());
    bookingRepository.save(booking);
    return new BookingResponse(BookingResponse.Status.BOOKED, booking.getId());
  }

  @Override
  @Transactional(readOnly = true)
  public Booking getBooking(long bookingId) {
    if (bookingId <= 0) {
      throw new IllegalArgumentException("Invalid booking ID. It must be greater than zero");
    }
    Optional<Booking> booking = bookingRepository.findById(bookingId);
    if (!booking.isPresent()) {
      throw new RecordNotFoundException("No booking with ID " + bookingId);
    }
    return booking.get();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Booking> getBookings(DateRange dateRange) {
    return bookingRepository.findAllByFromAfterAndUntilBefore(dateRange.getFrom(), dateRange.getUntil());
  }

  @Override
  @Transactional(readOnly = true)
  public List<Booking> getBookings(DateRange dateRange, RoomCategory category) {
    return bookingRepository
        .findAllByFromAfterAndUntilBeforeAndCategoryId(dateRange.getFrom(), dateRange.getUntil(), category.getId());
  }

  @Override
  public Booking getLastUpdatedBooking(DateRange dateRange) {
    Sort sort = Sort.by(Sort.Direction.DESC, "updatedAt");
    return bookingRepository.findOneByFromAfterAndUntilBefore(dateRange.getFrom(), dateRange.getUntil(), sort);
  }

  @Override
  public Booking getLastUpdatedBooking(DateRange dateRange, RoomCategory category) {
    Sort sort = Sort.by(Sort.Direction.DESC, "updatedAt");
    return bookingRepository
        .findOneByFromAfterAndUntilBeforeAndCategoryId(dateRange.getFrom(), dateRange.getUntil(), category.getId(), sort);
  }
}

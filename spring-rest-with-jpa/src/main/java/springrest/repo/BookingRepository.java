package springrest.repo;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import springrest.model.Booking;

import java.util.Date;
import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {

  public List<Booking> findAllByFromAfterAndUntilBefore(Date from, Date until);

  public Booking findOneByFromAfterAndUntilBefore(Date from, Date until, Sort sort);

  public List<Booking> findAllByFromAfterAndUntilBeforeAndCategoryId(Date from, Date until, Long categoryId);

  public Booking findOneByFromAfterAndUntilBeforeAndCategoryId(Date from, Date until, Long categoryId, Sort sort);

}

package springrest.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import springrest.model.Room;

public interface RoomRepository extends JpaRepository<Room, Long> {
}

package springrest.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import springrest.model.RoomCategory;

public interface RoomCategoryRepository extends JpaRepository<RoomCategory, Long> {

}

package springrest.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * This class represents a type of {@link Room}. Each room belongs to one room category.
 */
@Entity(name = "room_categories")
public class RoomCategory {

  private long id;
  private String name;
  private String description;
  private Pricing pricing;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long getId() {
    return id;
  }

  public RoomCategory setId(long id) {
    this.id = id;
    return this;
  }

  @Column(name = "name", unique = true, nullable = false, length = 128)
  public String getName() {
    return name;
  }

  public RoomCategory setName(String name) {
    this.name = name;
    return this;
  }

  @Column(name = "description", length = 2048)
  public String getDescription() {
    return description;
  }

  public RoomCategory setDescription(String description) {
    this.description = description;
    return this;
  }

  @OneToOne(cascade = CascadeType.ALL)
  public Pricing getPricing() {
    return pricing;
  }

  public RoomCategory setPricing(Pricing pricing) {
    this.pricing = pricing;
    return this;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }
}

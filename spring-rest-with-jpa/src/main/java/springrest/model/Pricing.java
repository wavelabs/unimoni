package springrest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Captures pricing for room categories.
 */
@Entity(name = "pricings")
public class Pricing {

  private long id;
  private PricingModel pricingModel;
  private Double priceGuest1;
  private Double priceGuest2;
  private Double priceGuest3;

  private Pricing() {
    // required for Hibernate
  }

  public Pricing(PricingModel pricingModel) {
    if (pricingModel == null) {
      throw new IllegalArgumentException("pricingModel");
    }
    this.pricingModel = pricingModel;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long getId() {
    return id;
  }

  public Pricing setId(long id) {
    this.id = id;
    return  this;
  }

  @Enumerated(EnumType.STRING)
  public PricingModel getPricingModel() {
    return pricingModel;
  }

  public Pricing setPricingModel(PricingModel pricingModel) {
    if (pricingModel == null) {
      throw new IllegalArgumentException("pricingModel");
    }
    this.pricingModel = pricingModel;
    return this;
  }

  @Column(name = "price_guest_1", nullable = false)
  public Double getPriceGuest1() {
    return priceGuest1;
  }

  public Pricing setPriceGuest1(Double priceGuest1) {
    this.priceGuest1 = priceGuest1;
    return this;
  }

  @Column(name = "price_guest_2")
  public Double getPriceGuest2() {
    return priceGuest2;
  }

  public Pricing setPriceGuest2(Double priceGuest2) {
    this.priceGuest2 = priceGuest2;
    return this;
  }

  @Column(name = "price_guest_3")
  public Double getPriceGuest3() {
    return priceGuest3;
  }

  public Pricing setPriceGuest3(Double priceGuest3) {
    this.priceGuest3 = priceGuest3;
    return this;
  }
}

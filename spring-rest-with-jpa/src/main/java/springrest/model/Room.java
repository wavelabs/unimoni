package springrest.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * This class represents a rental room on property management system.
 */
@Entity(name = "rooms")
public class Room {

  private long id;

  @ManyToOne(cascade = CascadeType.ALL)
  private RoomCategory roomCategory;
  private String name;
  private String description;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long getId() {
    return id;
  }

  public Room setId(long id) {
    this.id = id;
    return this;
  }

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
  public RoomCategory getRoomCategory() {
    return roomCategory;
  }

  public Room setRoomCategory(RoomCategory roomCategory) {
    this.roomCategory = roomCategory;
    return this;
  }

  @Column(name = "name", unique = true, nullable = false, length = 128)
  public String getName() {
    return name;
  }

  public Room setName(String name) {
    this.name = name;
    return this;
  }

  @Column(name = "description")
  public String getDescription() {
    return description;
  }

  public Room setDescription(String description) {
    this.description = description;
    return this;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }
}

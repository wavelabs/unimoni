package springrest.web;

import java.util.List;

/**
 * Our envelope.
 */
public class ApiResponse {

  private final Status status;
  private final Object data;
  private ApiError error;
  private List<ApiError> errors;

  public ApiResponse(Status status, Object data) {
    this(status, data, null);
  }

  public ApiResponse(Status status, Object data, ApiError error) {
    this.status = status;
    this.data = data;
    this.error = error;
  }

  public Status getStatus() {
    return status;
  }

  public Object getData() {
    return data;
  }

  public ApiError getError() {
    return error;
  }

  public static enum Status {
    OK,
    ERROR
  }

  public List<ApiError> getErrors() {
    return errors;
  }

  public ApiResponse setErrors(List<ApiError> errors) {
    this.errors = errors;
    return this;
  }

  public static final class ApiError {

    private final int errorCode;
    private final String description;
    private String field;

    public ApiError(int errorCode, String description) {
      this.errorCode = errorCode;
      this.description = description;
    }

    public int getErrorCode() {
      return errorCode;
    }

    public String getDescription() {
      return description;
    }

    public String getField() {
      return field;
    }

    public ApiError setField(String field) {
      this.field = field;
      return this;
    }
  }
}

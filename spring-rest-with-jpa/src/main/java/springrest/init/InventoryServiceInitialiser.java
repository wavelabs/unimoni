package springrest.init;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import springrest.DateRange;
import springrest.RecordNotFoundException;
import springrest.booking.BookingRequest;
import springrest.booking.BookingService;
import springrest.booking.CreditCardDetails;
import springrest.inventory.InventoryService;
import springrest.model.Booking;
import springrest.model.Pricing;
import springrest.model.PricingModel;
import springrest.model.Room;
import springrest.model.RoomCategory;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Utility class for initialising the inventory service.
 */
@Component("InventoryServiceInitialiser")
public class InventoryServiceInitialiser {

  @Autowired
  InventoryService inventoryService;

  @PostConstruct
  public void init() {
    try {
      inventoryService.getRoom(1);
    } catch (RecordNotFoundException e) {

      RoomCategory category = new RoomCategory();
      category.setName("Double Rooms");
      category.setDescription("Rooms with double beds");
      Pricing pricing = new Pricing(PricingModel.FIXED);
      pricing.setPriceGuest1(10.0);
      category.setPricing(pricing);

      inventoryService.getAllRoomsWithCategory(category);
    }
  }

}

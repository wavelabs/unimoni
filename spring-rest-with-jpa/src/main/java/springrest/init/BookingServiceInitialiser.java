package springrest.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import springrest.DateRange;
import springrest.booking.BookingRequest;
import springrest.booking.BookingService;
import springrest.booking.CreditCardDetails;
import springrest.model.Booking;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Initialiser for the booking service.
 */
@DependsOn("InventoryServiceInitialiser")
@Component
public class BookingServiceInitialiser {

  @Autowired
  BookingService bookingService;

  @PostConstruct
  public void init() {
    List<Booking> bookings = bookingService.getBookings(new DateRange());
//    if (bookings.isEmpty()) {
//      CreditCardDetails creditCardDetails = new CreditCardDetails("Jane Doe", "1111-11111-1111-1111", "08/19", "111");
//      bookingService.book(new BookingRequest(1, new DateRange(), "John Doe", creditCardDetails));
//    }
  }
}
